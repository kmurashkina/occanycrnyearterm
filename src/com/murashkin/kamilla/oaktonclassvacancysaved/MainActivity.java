package com.murashkin.kamilla.oaktonclassvacancysaved;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {
	
	EditText[]  crnNumber= new EditText[5];  //should be array now
	int     year; 
	int     term;  //10 for Spring; 20 for Summer ; 30 for Fall
	String  yearGroupChoice, termGroupChoice,yearGroupChoiceOld,termGroupChoiceOld;
	Spinner yearGroup;
	Spinner termGroup;
	SharedPreferences sharedPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate ( savedInstanceState );
		
		setContentView ( R.layout.activity_main );
		
		yearGroup = (Spinner)findViewById ( R.id.txtYearGroup );
		termGroup = (Spinner)findViewById ( R.id.txtTermGroup );
		
		crnNumber[0] = (EditText)findViewById( R.id.https0 );
		crnNumber[1] = (EditText)findViewById( R.id.https1 );
		crnNumber[2] = (EditText)findViewById( R.id.https2 );
		crnNumber[3] = (EditText)findViewById( R.id.https3 );
		crnNumber[4] = (EditText)findViewById( R.id.https4 );
		
		sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		
		crnNumber[0].setText ( sharedPref.getString("crn1", "") );
		crnNumber[1].setText ( sharedPref.getString("crn2", "") );
		crnNumber[2].setText ( sharedPref.getString("crn3", "") );
		crnNumber[3].setText ( sharedPref.getString("crn4", "") );
		crnNumber[4].setText ( sharedPref.getString("crn5", "") );
		
		yearGroupChoiceOld = sharedPref.getString("year", "");
		termGroupChoiceOld = sharedPref.getString("term", "");

		@SuppressWarnings("unchecked")
		int yearPosition = ((ArrayAdapter<String>) yearGroup.getAdapter()).getPosition(yearGroupChoiceOld);
		yearGroup.setSelection(yearPosition);
		
		@SuppressWarnings("unchecked")
		int termPosition = ((ArrayAdapter<String>) termGroup.getAdapter()).getPosition(termGroupChoiceOld);
		termGroup.setSelection(termPosition);
		
		Button b = (Button)this.findViewById(R.id.go_btn);
		b.setOnClickListener(this);
	}
	
	//                                                     < args type, ????, result-type >
	private class ProcessClassInformation extends AsyncTask< String,    Void, String      > 
	{
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() */
	    protected String doInBackground ( String... urls )
	    {
	    	String result = new String();  
	    	result+="Year "+yearGroupChoice+ " Term "+ termGroupChoice+"\n";
	    	
	    	String temp, temp2;
	    	for (int i=0; i<urls.length; ++i )
	    	{
	    		temp2 = "" + crnNumber[i].getText();
	    		if (temp2.compareTo("")==0) continue;
	    		temp    = getClassInformation ( urls[i] );
	    		result += (i+1>9?""+(i+1):"0"+(i+1))+ " CRN ";
	    		if ((temp.substring(0,35)).compareTo("No detailed class information found")==0) 
	    		{ 
	    			result += ""+crnNumber[i].getText()+ " ";
	    		}
	    		result += temp;
	    	}
	        return result; // returns result to be used in onPostExecute
	    }
	    
	    /** The system calls this to perform work in the UI thread and delivers
	      * the result from doInBackground() */
	    protected void onPostExecute(String result)
	    {
            final TextView resp = (TextView) findViewById(R.id.response);
            resp.setText(result);
	    }
	}
		
    @Override
    public void onClick(View v)
    {
    	yearGroupChoice = yearGroup.getSelectedItem().toString();
    	termGroupChoice = termGroup.getSelectedItem().toString();
    	
    	if (termGroupChoice.compareTo("Spring")==0)
    	{
    		term = 10;
    	}
    	else if (termGroupChoice.compareTo("Summer") == 0)
    	{
    		term = 20;
    	}
    	else 
    	{
    		term = 30;
    	}
    	
    	SharedPreferences.Editor editor = sharedPref.edit();
    	
		editor.putString ( "year", yearGroupChoice );
		editor.putString ( "term", termGroupChoice );
		
    	String[] urlStr = new String[5];
    	String[] keys   = {"crn1","crn2","crn3","crn4","crn5"};
    	String strCrn;
    	for ( int i=0; i<5; ++i )
    	{
    		strCrn=crnNumber[i].getText().toString();
    		editor.putString(keys[i], strCrn);
    	}
    	
    	editor.commit();
    	
    	for (int i=0;i<5;++i)
    	{
    	    urlStr[i]="https://ssb.oakton.edu/BPRD/bwckschd.p_disp_detail_sched?term_in="
    	    	+ yearGroupChoice
    	    	+ term
    	    	+ "&crn_in="
    	    	+ crnNumber[i].getText();   //should take from array of crns
    	}
    	
    	Log.d ( "MainActivity", "onClick "+urlStr ); 

        new ProcessClassInformation().execute(urlStr);
    }
    
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
    	
    	if (keyCode == KeyEvent.KEYCODE_BACK) {

    		return true;
    	}
    	return super.onKeyUp(keyCode, event);
    }
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
    private String getPageAsStringByUrl(String url)
    {  
    	Log.d( "MainActivity", "getPageAsStringByUrl " + url ); 

    	String output = null;
    	
    	DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse = null;
		try {
			httpResponse = httpClient.execute(httpGet);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        HttpEntity httpEntity = httpResponse.getEntity();
        try {
			output = EntityUtils.toString(httpEntity);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
    	return output;
    }
    
    public Students getNumberOfStudents(String output)
    {  
    	String courseTitle   = "";
        String errorMessage  = "";
    	int    indexOfActual = output.indexOf("Seats");
    	int    first         = output.indexOf("dddefault",indexOfActual);

    	String numberOfCapacity;
    	if (output.substring(first+12,first+13).equals("<")){
    		numberOfCapacity = output.substring(first+11,first+12);
    	}
    	else
    	{
    		numberOfCapacity = output.substring(first+11,first+13);
    	}
    	
    	int    second = output.indexOf("dddefault", first+10);
    	String numberOfActualStudents;
    	if (output.substring(second+12,second+13).equals("<")){
    		numberOfActualStudents = output.substring(second+11,second+12);
    	}
    	else
    	{
    		numberOfActualStudents = output.substring(second+11,second+13);
    	}
    	
    	int third=output.indexOf("dddefault", second+10);       
    	String numberRemaining;
    	if (output.substring(third+12,third+13).equals("<")){
    		numberRemaining = output.substring(third+11,third+12);
    	}
    	else
    	{
    		numberRemaining = output.substring(third+11,third+13);
    	}
    	
    	//finding courseTitle
    	int indexOfCourseTitle = output.indexOf("CLASS");//="ddlabel" scope="row" >");
    	int fourth  = output.indexOf("ddlabel",indexOfCourseTitle);
    	int fourth2 = output.indexOf("row",fourth); 
    	int fourth3 = output.indexOf(">",fourth2);
    	int fourth4 = output.indexOf("<",fourth3);
    	courseTitle = output.substring(fourth3+1,fourth4);
    	
    	//finding errorMessage
    	int indexOferrorMessage = output.indexOf("errortext");//="ddlabel" scope="row" >");    	
    	int fifth               = output.indexOf(">",indexOferrorMessage);
    	int fifth2              = output.indexOf("<",fifth); 
    	errorMessage            = output.substring(fifth+1,fifth2);	    	
    	
    	Students stud = new Students(
    			numberOfCapacity,
    			numberOfActualStudents,
    			numberRemaining,
    			courseTitle,
    			errorMessage
    	);
    	
    	return stud;
    }
    
    private String getClassInformation( final String url ) {
    
	    String   output   = getPageAsStringByUrl(url);
		Students students = getNumberOfStudents(output);
		
	    return students.toString();
    }           
}
