package com.murashkin.kamilla.oaktonclassvacancysaved;

public class Students {
	
	private String capacity;
	private String actual;
	private String remaining;
	private String courseTitle;
	private String errorMessage = "";
	
	public Students(String capacity, String actual, String remaining, String courseTitle,String errorMessage)
	{
		this.capacity     = capacity;
		this.actual       = actual;
		this.remaining	  = remaining;
		this.courseTitle  = courseTitle;
		this.errorMessage = errorMessage;
	}
    
	private String num( String value ) {
		
		return value.length() > 1 ? value : "0"+value;
	}
	
	public String toString()
	{	
		if (capacity.compareTo("HT")!=0)
	    {
			int m = 0;
			while (m<courseTitle.length()) {
				m = courseTitle.indexOf(" - ",m);
				if ( courseTitle.charAt(m+3) >= '0' && courseTitle.charAt(m+3) <= '9' ) break;
				m += 3;
			}
			
			return String.format("%s cap %-2s act %-2s rem %-2s %s\n", 
					courseTitle.substring(m+3), num(capacity), num(actual), num(remaining), courseTitle.substring(0,m));	
	    }
		else
		{
			return errorMessage+"\n";
		}
	}
	
	public String toStringOld()
	{	
		if (capacity.compareTo("HT")!=0)
	    {
		    return 
				courseTitle
				+" capacity="+capacity
				+" actual=" +actual
				+" remaining="+remaining;
		} else {
			return errorMessage;
		}	       
	}
	
	public String getCapacity()
	{
		return capacity;
	}
	
	public String getActual()
	{
		return actual;
	}
	
	public String getRemaining()
	{
		return remaining;
	}
	
	public String getCourseTitle()
	{
		return courseTitle;
	}
	
	public String getErrorMessage()
	{
		return errorMessage;
	}
}
